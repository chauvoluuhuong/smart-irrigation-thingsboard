import * as React from "react";
import { useState, useEffect } from "react";
import { CssVarsProvider } from "@mui/joy/styles";
import CssBaseline from "@mui/joy/CssBaseline";
import Box from "@mui/joy/Box";
import Typography from "@mui/joy/Typography";
import IconButton from "@mui/joy/IconButton";

import Button from "@mui/joy/Button";
// custom
import Layout from "./components/Layout";
import AppRouterProvider from "./router";
import UserType from "./types/userType";
import HomeIcon from "@mui/icons-material/Home";
import Link from "@mui/material/Link";
const App = function () {
  const [user, setUser] = useState<UserType>({});

  const renderHeader = () => (
    <Layout.Header>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          gap: 1.5,
        }}
      >
        <IconButton
          size="sm"
          variant="soft"
          sx={{ display: { xs: "none", sm: "inline-flex" } }}
        >
          <HomeIcon />
        </IconButton>
        <Link></Link>
        <Typography component="h1" fontWeight="xl">
          <Link color="inherit" href="/">
            Home
          </Link>
        </Typography>
      </Box>
      <Box sx={{ display: "flex", flexDirection: "row", gap: 1.5 }}>
        <Button
          onClick={(e) => {
            e.preventDefault();
            window.location.href = "/login";
          }}
        >
          Login
        </Button>
      </Box>
    </Layout.Header>
  );

  return (
    <CssVarsProvider disableTransitionOnChange>
      <CssBaseline />
      <Layout.Root>
        {renderHeader()}
        <AppRouterProvider user={user} setUser={setUser} />
      </Layout.Root>
    </CssVarsProvider>
  );
};

export default App;
