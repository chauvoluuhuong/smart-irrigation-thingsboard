import * as React from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import { useState } from "react";
import UserType from "./types/userType";
interface Props {
  user: UserType;
  setUser: (user: UserType) => void;
}

const AppRouterProvider = ({ user, setUser }: Props) => {
  const setRouterWitUser = () =>
    Object.keys(user).length
      ? [
          {
            path: "/",
            element: <Home user={user} />,
          },
          {
            path: "/login",
            element: <Login user={user} setUser={setUser} />,
          },
        ]
      : [
          {
            path: "*",
            element: <Login user={user} setUser={setUser} />,
          },
        ];

  const [router, setRouter] = useState(createBrowserRouter(setRouterWitUser()));

  React.useEffect(() => {
    setRouter(createBrowserRouter(setRouterWitUser()));
  }, [user]);
  return <RouterProvider router={router} />;
};

export default AppRouterProvider;
