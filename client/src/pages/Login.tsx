import * as React from "react";
import Sheet from "@mui/joy/Sheet";
import Typography from "@mui/joy/Typography";
import FormControl from "@mui/joy/FormControl";
import FormLabel from "@mui/joy/FormLabel";
import Input from "@mui/joy/Input";
import Button from "@mui/joy/Button";
import UserType from "../types/userType";
import Grid from "@mui/joy/Grid";
import axios from "../configuredAxios";
import { useNavigate } from "react-router-dom";
import { Box } from "@mui/joy";
import { getUrlParams } from "../util";
interface Props {
  user: UserType;
  setUser: (user: UserType) => void;
}

export default function Login({ user, setUser }: Props) {
  const [thingBoardAPItUrl, setThingsBoardAPIUrl] = React.useState("");
  const [username, setUserName] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [errMs, setErrMs] = React.useState("");
  const navigate = useNavigate();
  const login = () => {
    if (!thingBoardAPItUrl) alert("please input thingsboard API URL first");
    else
      axios
        .post("/auth/login", {
          username,
          password,
        })
        .then((r) => {
          console.log(r.data);
          localStorage.setItem("jwt", r.data.token);
          return r.data.token;
        })
        .then((r) => {
          getUserInfo(r);
        })
        .catch((e) => {
          setErrMs(e.message);
        });
  };

  const getUserInfo = (jwt: string) => {
    axios
      .get("/auth/user", {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      })
      .then((r) => {
        setUser(r.data);
        navigate(`/?deviceId=${getUrlParams().get('deviceId')}`);
      })
      .catch((e) => console.log(e));
  };

  React.useEffect(() => {
    const _thingBoardAPItUrl = localStorage.getItem("thingBoardAPItUrl");
    if (_thingBoardAPItUrl) {
      setThingsBoardAPIUrl(_thingBoardAPItUrl);
      axios.defaults.baseURL = _thingBoardAPItUrl;
    }
    const jwt = localStorage.getItem("jwt");
    if (jwt) {
      axios.defaults.headers.common["Authorization"] = `Bearer ${jwt}`;
      if (_thingBoardAPItUrl) {
        getUserInfo(jwt);
      }
    }
  }, []);
  return (
    <Grid sx={{ gridColumn: "1 / -1" }} container justifyContent={"center"}>
      <Grid xs={12} md={4}>
        <Sheet
          sx={{
            mx: "auto", // margin left & right
            my: 4, // margin top & bottom
            py: 3, // padding top & bottom
            px: 2, // padding left & right
            display: "flex",
            flexDirection: "column",
            gap: 2,
            borderRadius: "sm",
            boxShadow: "md",
          }}
          variant="outlined"
        >
          <div>
            <Typography level="h4" component="h1">
              <b>Welcome!</b>
            </Typography>
            {errMs ? (
              <Typography sx={{ color: "red" }} level="h4" component="h1">
                <b>{errMs}</b>
              </Typography>
            ) : null}
            <Typography level="body-sm">
              {user.email ? user.email : "please login"}
            </Typography>
          </div>
          <FormControl>
            <FormLabel>User name</FormLabel>
            <Input
              onChange={(e) => setUserName(e.target.value)}
              name="user name"
            />
          </FormControl>
          <FormControl>
            <FormLabel>Password</FormLabel>
            <Input
              onChange={(e) => setPassword(e.target.value)}
              // html input attribute
              name="password"
              type="password"
              placeholder="password"
            />
          </FormControl>
          <FormControl>
            <FormLabel>ThingBoard Host Url</FormLabel>
            <Input
              value={thingBoardAPItUrl}
              onChange={(e) => {
                setThingsBoardAPIUrl(e.target.value);
                localStorage.setItem("thingBoardAPItUrl", e.target.value);
              }}
              // html input attribute
              name="Thingsboard API Url"
              placeholder="Thingsboard API Url"
            />
          </FormControl>

          <Box display={"flex"}>
            <Button fullWidth sx={{ mt: 1, mr: 1 }} onClick={login}>
              Log in
            </Button>
            <Button
              color="danger"
              fullWidth
              sx={{ mt: 1 }}
              onClick={() => {
                localStorage.setItem("jwt", "");
                setUser({});
              }}
            >
              Log out
            </Button>
          </Box>
        </Sheet>
      </Grid>
    </Grid>
  );
}
