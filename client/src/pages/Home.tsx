import * as React from "react";
import { Box } from "@mui/joy";

import Button from "@mui/joy/Button";
import DayScheduler from "../components/DayScheduler";
import axios from "axios";
import DevicesList from "../components/DevicesList";
import UserType from "../types/userType";
import DeviceType from "../types/Devices";
import { SchedulerType } from "../types/Scheduler";
import Grid from "@mui/joy/Grid";
import { getUrlParams } from "../util";

interface Props {
  user: UserType;
}

const Home = ({ user }: Props) => {
  const [devices, setDevices] = React.useState([]);
  const [devicesSelected, setDeviceSelected] = React.useState<DeviceType>();
  const [scheduler, setScheduler] = React.useState<SchedulerType>({
    days: [],
  });

  const getDevices = () => {
    axios
      .get(`/customer/${user.customerId?.id}/deviceInfos?pageSize=10&page=0`)
      .then((r) => {
        setDevices(r.data.data);
        const deviceId = getUrlParams().get("deviceId");
        if (deviceId) {
          for (let d of r.data.data) {
            if (d.id.id === deviceId) setDeviceSelected(d);
          }
        }
      })
      .catch((e) => alert(`can't get devices: ${e.message}`));
  };

  const setWateringTime = (
    dayId: number,
    timeId: number,
    timeKey: string,
    val: string
  ) => {
    const _sch = JSON.parse(JSON.stringify(scheduler));
    _sch.days[dayId].wateringTimes[timeId][timeKey] = val;
    setScheduler(_sch);
  };

  const addWateringTime = (dayId: number) => {
    const _sch = JSON.parse(JSON.stringify(scheduler));
    _sch.days[dayId].wateringTimes.push({
      startTime: "",
      endTime: "",
    });
    setScheduler(_sch);
  };

  const removeWateringTime = (dayId: number) => {
    const _sch = JSON.parse(JSON.stringify(scheduler));
     _sch.days[dayId].wateringTimes.pop();
    setScheduler(_sch);
  };

  const removeDay = (dayId: number) => {
    const _sch = JSON.parse(JSON.stringify(scheduler));
     _sch.days.splice(dayId, 1)
    setScheduler(_sch);
  };

  const handleAddDayScheduler = () => {
    setScheduler({
      days: [
        ...scheduler.days,
        ...[
          {
            wateringTimes: [
              {
                startTime: "",
                endTime: "",
              },
            ],
          },
        ],
      ],
    });
  };

  const handleSubmitScheduling = () => {
    console.log("scheduler: ", scheduler);
    if (devicesSelected?.id?.id)
      axios
        .post(
          `plugins/telemetry/DEVICE/${devicesSelected.id.id}/SHARED_SCOPE`,
          { scheduler }
        )
        .then((r) => alert("submit scheduling successfull"))
        .catch((e) => alert(e.message));
  };

  const renderDayscheduler = () =>
    scheduler.days.map((d, id) => (
      <Grid xs={12} md={4} lg={4} key={id}>
        <DayScheduler
          dayScheduler={scheduler.days[id]}
          dayId={id}
          setWateringTime={setWateringTime}
          addWateringTime={addWateringTime}
          removeWateringTime={removeWateringTime}
          removedDay={removeDay}
        />
      </Grid>
    ));

  const getScheduler = (deviceId: string) => {
    // from shared attribute scope
    axios
      .get(
        `/plugins/telemetry/DEVICE/${deviceId}/values/attributes/SHARED_SCOPE`
      )
      .then((r) => {
        if (r.data) {
          for (let d of r.data) {
            if (d.key === "scheduler") setScheduler(d.value);
          }
        }
        console.log(r);
      });
  };

  React.useEffect(() => {
    if (devicesSelected?.id?.id) getScheduler(devicesSelected.id.id);
  }, [devicesSelected]);

  React.useEffect(() => {
    // initialize hook
    getDevices();
  }, []);

  React.useEffect(() => {});
  return (
    <>
      <Box
        sx={{
          gridColumn: "1 / -1",
          p: 2,
          display: "flex",
          gap: 2,
          flexWrap: "wrap",
        }}
      >
        <Box sx={{ flex: 1 }}>
          <DevicesList
            deviceSelected={devicesSelected}
            devices={devices}
            setDeviceSelected={setDeviceSelected}
          />
        </Box>
        {/* <Input /> */}

        <Button onClick={handleAddDayScheduler}>Add day scheduler</Button>
        <Button onClick={handleSubmitScheduling}>Submit scheduling</Button>
        <Button>Start watering</Button>
      </Box>
      <Box sx={{ gridColumn: "1/-1" }}>
        <Grid container spacing={2} sx={{ pl: 2, pr: 2 }}>
          {renderDayscheduler()}
        </Grid>
      </Box>
    </>
  );
};

export default Home;
