import * as React from "react";
import {
  Box,
  Card,
  CardActions,
  CardContent,
  Typography,
  Button,
} from "@mui/joy";
import Input from "@mui/joy/Input";
import { DaySchedulerType } from "../types/Scheduler";
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import AddCircleIcon from '@mui/icons-material/AddCircle';

interface Props {
  dayId: number;
  dayScheduler: DaySchedulerType;
  setWateringTime: (
    dayId: number,
    timeId: number,
    timeKey: string,
    val: string
  ) => void;
  addWateringTime: (dayId: number) => void,
  removeWateringTime: (dayId: number) => void
  removedDay: (dayId: number) => void
}

export default ({ dayId, setWateringTime, dayScheduler, addWateringTime, removeWateringTime, removedDay }: Props) => {

  const renderTiming = () =>
    dayScheduler?.wateringTimes?.map((w, i) => (
      <>
        <Box
          sx={{
            display: "flex",
            flex: 1,
            gap: 1,
          }}
        >
          <Input
            value={w.startTime}
            onChange={e => setWateringTime(dayId, i, 'startTime', e.target.value)}
            fullWidth
            placeholder="Time start"
            type="time"
          />
          <Input
            value={w.endTime}
            onChange={e => setWateringTime(dayId, i, 'endTime', e.target.value)}
            fullWidth
            placeholder="Time start"
            type="time"
          />
        </Box>
      </>
    ));
  return (
    <Card>
      <CardContent
        sx={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          gap: 1,
        }}
      >
        <Typography level="h4">Day {dayId}</Typography>
        {renderTiming()}
      </CardContent>
      <CardActions>
        <Button endDecorator={<AddCircleIcon />} onClick={() => {addWateringTime(dayId)}}>Add time</Button>
        <Button color="danger" endDecorator={<RemoveCircleIcon />} onClick={() => removeWateringTime(dayId)}>Remove time</Button>
        <Button color="danger" endDecorator={<RemoveCircleIcon />}  onClick={() => removedDay(dayId)}>Remove day</Button>
      </CardActions>
    </Card>
  );
};
