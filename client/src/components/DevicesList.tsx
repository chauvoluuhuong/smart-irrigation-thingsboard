import * as React from "react";
import Autocomplete from "@mui/joy/Autocomplete";
import { Typography } from "@mui/joy";
import DeviceType from "../types/Devices";

interface Props {
  devices: Array<DeviceType>;
  deviceSelected?: DeviceType;
  setDeviceSelected: (device: DeviceType) => void;
}

export default function DevicesList({
  devices,
  setDeviceSelected,
  deviceSelected,
}: Props) {
  const buildDeviceOptions = () =>
    devices.map((d) => ({
      ...d,
      label: d.name,
    }));
  return (
    <Autocomplete
      value={deviceSelected}
      onChange={(e, value) => {
        if (value) setDeviceSelected(value);
      }}
      startDecorator={<Typography>Select your device: </Typography>}
      placeholder={deviceSelected ? `showing scheduler of ${deviceSelected.name}` : 'select your device'}
      options={buildDeviceOptions()}
    />
  );
}
