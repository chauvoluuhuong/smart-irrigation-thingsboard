export default interface UserType {
  email?: string;
  name?: string;
  ownerId?: {
    entityType: string;
    id: string;
  };
  customerId?: {
    entityType: string;
    id: string;
  };
}
