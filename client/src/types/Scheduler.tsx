export interface TimingType {
  startTime: string;
  endTime: string;
}

export interface DaySchedulerType {
  wateringTimes?: Array<TimingType>;
}

export interface SchedulerType {
  days: Array<DaySchedulerType>;
}
