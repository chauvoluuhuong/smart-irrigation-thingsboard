export default interface DeviceType {
  id?: {
    entityType: string;
    id: string;
  };
  name: string;
}
